import { LogLevelDescType, Logger } from "loglevel-colored-prefix";
import { BioSeqSet, FastaUtils } from "bioseq-ts/lib";
import { CommandToolKit } from 'blastinutils-ts'
import fs from "fs";
import { IMakeBlastDBParameters, IBlastPParameters, IOutputFormat, ILink } from "blastinutils-ts/lib/interfaces";
import { promisify } from "util";
import childProcess from "child_process";
import { WritableParser, ISupportedParams } from "blastinutils-ts/lib/BlastToLinksStream";

const OUTFMT: IOutputFormat = {
  format: 6,
  parameters: [
    'qseqid',
    'sseqid',
    'evalue',
  ]
}

interface IResults {
  links: ILink[]
  name: string
  nodes: string[]
}

class DynoCluster {
  protected readonly dataset: BioSeqSet
  protected readonly loglevel: LogLevelDescType
  protected readonly log: Logger
  protected projectCode: string
  protected names:  {[name: string]: string}
  protected commands: { makeDb: string, blastp: string }

  constructor(dataset: BioSeqSet, loglevel: LogLevelDescType = 'info') {
    this.dataset = dataset
    this.loglevel = loglevel;
    this.commands = {
      blastp: "",
      makeDb: ""
    }
    this.log = new Logger(loglevel)
    this.projectCode = 'dc'
    this.names = {}
  }

  public async generate () {
    this.projectCode += `.${this.dataset.getHash().substr(0,10)}`
    this.names.fastaTmp = `${this.projectCode}.fa`
    this.names.db = `${this.projectCode}.db`
    this.names.blastOut = `${this.projectCode}.blastp.fmt6.tbl`
    const step1 = await this.makeFasta().generateCommands()
      .runCommand(this.commands.makeDb)
    await step1.runCommand(this.commands.blastp)
    return this.parseBlastOutput()
  }

  protected makeFasta(): this {
    const fu = new FastaUtils()
    const fasta = fu.write(this.dataset)
    fs.writeFileSync(this.names.fastaTmp, fasta)
    return this
  }

  protected generateCommands(): this {
    this.generateMakeBlastDb()
      .generateBlastP()
    return this
  }

  protected generateMakeBlastDb() {
    const ctk = new CommandToolKit(this.loglevel)
    const params: IMakeBlastDBParameters = {
      in: this.names.fastaTmp,
      dbtype: 'prot',
      out: this.names.db
    }
    this.commands.makeDb = ctk.build('makeblastdb', params)
    return this
  }

  protected generateBlastP() {
    const ctk = new CommandToolKit(this.loglevel)
    const params: IBlastPParameters = {
      query: this.names.fastaTmp,
      db: this.names.db,
      out: this.names.blastOut,
      outfmt: OUTFMT
    }
    this.commands.blastp = ctk.build('blastp', params)
    return this
  }

  /**
   * Helper function to launch third party cli programs.
   *
   * @private
   * @param {string} command
   * @returns
   * @memberof Engine
   */
  protected async runCommand(command: string) {
    const log = this.log.getLogger("DynoCluster::runComand");
    const exec = promisify(childProcess.exec);
    log.info(`Running: ${command}`)
    const { stdout, stderr } = await exec(command);
    if (stderr) {
      log.error(stderr);
      throw new Error(stderr);
    }
    if (stdout) {
      log.info(stdout);
    }
    return this;
  }

  protected async parseBlastOutput(): Promise<IResults> {
    const log = this.log.getLogger("DynoCluster::parseBlastOutput");
    log.info("Starting parseBlastOutput step");
    const index = this.dataset.getBioSeqs().map(e => e.header)
    const parser = new WritableParser(index, OUTFMT as ISupportedParams, this.loglevel);
    return new Promise((resolve, reject) => {
      fs.createReadStream(this.names.blastOut)
        .pipe(parser)
        .on("finish", () => {
          log.info("All parsed");
          const data: IResults = {
            name: this.projectCode,
            nodes: index,
            links: parser.getData()
          }
          resolve(data);
          return;
        })
        .on("error", (err: any) => {
          log.error(err);
          reject(err);
          return;
        });
    });
  };

}

export {DynoCluster}