#!/usr/bin/env node
// tslint:disable: no-console

import { ArgumentParser } from "argparse";
import chalk from "chalk";
import figlet from "figlet";
import fs from "fs";
import zlib from 'zlib'
import { FastaUtils} from 'bioseq-ts'

import { DynoCluster } from "../DynoCluster";

// tslint:disable-next-line: no-var-requires no-require-imports
const pkjson = require("../../package.json");
const splash = figlet.textSync("DynoCluster", {
  font: "Larry 3D",
  horizontalLayout: "fitted"
});

console.log(chalk.cyan(splash));
console.log(`\t\t\t\t\t\t\t\t  ${chalk.cyan("" + pkjson.version)}`);
console.log(chalk.red("\t\t\t\t\t\t\t by Davi Ortega"));

const parser = new ArgumentParser({
  addHelp: true,
  description:
    "Command line application to generate the data for Dyno Cluster visualization program.",
  prog: "dynocluster-app"
});

parser.addArgument("fasta", {
  help: "Fasta file"
});

const args = parser.parseArgs();

const fasta = fs.readFileSync(args.fasta).toString()
const fu = new FastaUtils()
const bioseqset = fu.parse(fasta)
const dc = new DynoCluster(bioseqset);

(async () => {
  const data = await dc.generate()
  const filename = `${data.name}.json.gz`
  const buf = zlib.gzipSync(JSON.stringify(data));
  fs.writeFileSync(filename, buf);
})()
