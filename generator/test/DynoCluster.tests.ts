import fs from 'fs'
import { DynoCluster } from '../src/DynoCluster'
import { FastaUtils, BioSeqSet } from 'bioseq-ts/lib'
import { expect } from 'chai'
import glob from 'glob'


process.chdir('./test')
const fasta = fs.readFileSync('test.fa').toString()

describe('DynoCluster', () => {
  describe('generate', () => {
    it('should work with default', async () => {
      const fu = new FastaUtils()
      const bioseqset = fu.parse(fasta)
      const dc = new DynoCluster(bioseqset)
      const results = await dc.generate()
      expect(results).to.haveOwnProperty('links')
      expect(results).to.haveOwnProperty('nodes')
    })
  })
  after(() => {
    const files = glob.sync('dc.*')
    files.forEach((file) => {
      try {
        fs.unlinkSync(file)
      }
      catch(err) {
        return
      }
    })
  })
})