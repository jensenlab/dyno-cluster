#!/usr/bin/env node

const { loadNuxt } = require('nuxt')

process.chdir(`${__dirname}/..`)

const main = async () => {
  const nuxt = await loadNuxt('start')
  await nuxt.listen('3000')
}

main()
